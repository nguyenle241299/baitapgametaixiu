import React, { Component } from 'react'
import { connect } from 'react-redux';
import { xucXacReducer } from './redux/reducers/xucXacReducer';
export class KetQua extends Component {
  render() {
    return (
      <div>
        <div className='display-4'>Bạn chọn: <span className='text-warning display-4'></span>{this.props.taiXiu ? "TAI" : "XIU"}</div>
        <div className='display-4'>Bàn thắng: <span className='text-primary'></span>{this.props.banThang}</div>
        <div className='display-4'>Tổng số bàn chơi: <span className='text-danger'></span>{this.props.tongSoBanChoi}</div>
        <button style={{
            fontSize: "40px"
        }} onClick = {this.props.handleClick} className='btn btn-success mt-3'>Play game</button>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
    return {
        taiXiu: state.xucXacReducer.taiXiu,
        banThang: state.xucXacReducer.banThang,
        tongSoBanChoi: state.xucXacReducer.tongSoBanChoi
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        handleClick: () => {
            dispatch({
                type: "PLAY_GAME",
            })
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(KetQua);