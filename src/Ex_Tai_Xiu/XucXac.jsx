
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { xucXacReducer } from './redux/reducers/xucXacReducer';

export class XucXac extends Component {

    renderXucXacList = () => {
        return this.props.mangXucXac.map((item, index) => {
            return <img
             key={index}
             src={item.img} 
             style = {{
                width: "70px",
                height: "70px",
                margin: 10
             }}   
             />
        })
    }

  render() {
    return (
      <div>
        <div>{this.renderXucXacList()}</div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
    return {
        mangXucXac: state.xucXacReducer.mangXucXac
    }
}


export default connect(mapStateToProps)(XucXac)
