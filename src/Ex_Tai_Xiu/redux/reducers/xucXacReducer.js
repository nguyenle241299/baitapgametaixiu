const initialState = {
    mangXucXac: [
        {
            img: "./imgXucSac/1.png",
            ma: 1
        },
        {
            img: "./imgXucSac/1.png",
            ma: 1
        },
        {
            img: "./imgXucSac/1.png",
            ma: 1
        }
    ],
    taiXiu: true,
    banThang: 0,
    tongSoBanChoi:0
}

export let xucXacReducer = (state = initialState, action) => {
    switch(action.type) {
        case "DAT_CUOC": {
            state.taiXiu = action.taiXiu
            return {...state}
        }

        case "PLAY_GAME": {
            let newMangXucXac = [];
            for(let i = 0; i < 3; i++) {
                let random = Math.floor(Math.random() * 6) + 1;
                let xucXacRandom = {
                    img: `./imgXucSac/${random}.png`,
                    ma: random
                }
                newMangXucXac.push(xucXacRandom)
            }

            state.mangXucXac = newMangXucXac;

            // xử lý tổng số bàn chơi
            state.tongSoBanChoi += 1;

            //  xử lý số bàn thắng
            let tongSoDiem = state.mangXucXac.reduce((tongDiem, xucXac, index) => {
                return tongDiem += xucXac.ma;
            }, 0)

            if((tongSoDiem > 11 && state.taiXiu == true) || (tongSoDiem <= 11 && state.taiXiu == false)) {
                state.banThang += 1;
            }
            return {...state}

        }

        default:
        return state;
    }
}