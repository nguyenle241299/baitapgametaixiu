import React, { Component } from 'react'
import KetQua from './KetQua'
import XucXac from './XucXac'
import "./game.css"
import bgGame from "../asset/bgGame.png"
import { connect } from 'react-redux';

export class Ex_TaiXiu extends Component {
  render() {
    return (
      <div style={{
        background: `url(${bgGame})`,
        width: "100vw",
        height: "100vh"
      }} className='bg-game pt-5 text-center'>
        <div className='row'>
            <div className='col-4'>
                <button style={{
                    width: "100px",
                    height: "100px",
                    fontSize: 30
                }} 
                onClick = {() => {
                    this.props.dat_cuoc(true)
                }}
                className='btn btn-success'>Tài</button>
            </div>
            <div className="col-4">
                <XucXac/>
            </div>
            <div className="col-4">
                <button style={{
                    width: "100px",
                    height: "100px",
                    fontSize: 30
                }}
                onClick = {() => {
                    this.props.dat_cuoc(false)
                }} 
                className='btn btn-danger'>Xỉu</button>
            </div>
        </div>
        <div className="text-center">
            <KetQua/>
        </div>
      </div>
    )
  }
}

const mapDispatchToProps = (dispatch) => {
    return {
        dat_cuoc: (taiXiu) => { 
            dispatch({
                type: "DAT_CUOC",
                taiXiu
            })
         }
    }
}

export default connect(null, mapDispatchToProps)(Ex_TaiXiu)
 